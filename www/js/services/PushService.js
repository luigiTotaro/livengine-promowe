angular.module('LiveEngine.Push.Services', [])

.service('PushServices', function(GlobalVariables, $rootScope, $http, $ionicModal, $ionicPopup) {

    var me = this;
    me.PUSH_NOTIFICATION_REGISTRATION_ID = null;
    me.onRegisteredCallback = null;




    me.setOnRegisteredCallback = function(registeredCallback) {

      me.onRegisteredCallback = registeredCallback;
      
      if(me.PUSH_NOTIFICATION_REGISTRATION_ID != null) {
          me.onRegisteredCallback(me.PUSH_NOTIFICATION_REGISTRATION_ID);
      }

    }

    me.InitPushNotifications = function() {


        if(!window.cordova) {
            return true;
        }


        var push = PushNotification.init({

            android: {
                senderID: GlobalVariables.senderID,
                clearBadge: 'true',
                forceShow: true,
                sound: true,
                vibrate: true
            },

            ios: {
                alert: "true",
                badge: "true",
                sound: "true",
                clearBadge: true
            }

        });



        push.on('registration', function(data) {
            // data.registrationId
            
            me.PUSH_NOTIFICATION_REGISTRATION_ID = data.registrationId;
            
            if(me.onRegisteredCallback) {
                me.onRegisteredCallback(data.registrationId);
            }

        });



        push.on('notification', function(data) {

          // alert('received notification: ' + data.message);
          // data.message,
          // data.title,
          // data.count,
          // data.sound,
          // data.image,
          // data.additionalData
        });


        push.on('error', function(e) {

            me.PUSH_NOTIFICATION_REGISTRATION_ID = null;
        
        });

    };




})
