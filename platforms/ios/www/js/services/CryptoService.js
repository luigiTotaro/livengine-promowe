angular.module('LiveEngine.Crypto.Services', [])

.service('CryptoService', function(GlobalVariables, $rootScope, $http, $ionicModal, $ionicPopup) {

    var me = this;
    
    me.encode = function(s) {
      var k = "123";
      var enc = "";
      var str = "";
      //console.log("key: " + k);
      // make sure that input is string
      str = s.toString();
      for (var i = 0; i < s.length; i++) {
        // create block
        var a = s.charCodeAt(i);
        // bitwise XOR
        var b = a ^ k;
        enc = enc + String.fromCharCode(b);
      }
      return enc;

    };

    me.decode = function(s) {
      //se riesco a fare un JSON.parse, vuol dire che quello che sto tentando di decodificare è già in chiaro
      //(possibile decodifica di un vecchio file in chiaro in assenza di rete, per esempio)

      try {
          JSON.parse(s);
      } catch (e) {

        var k = "123";
        var enc = "";
        var str = "";
        //console.log("key: " + k);
        // make sure that input is string
        str = s.toString();
        for (var i = 0; i < s.length; i++) {
          // create block
          var a = s.charCodeAt(i);
          // bitwise XOR
          var b = a ^ k;
          enc = enc + String.fromCharCode(b);
        }
        return enc;

      }
      return s;

    };



})
