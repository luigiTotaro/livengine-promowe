function MarkerIndoor(poiData) {

    /*
        For creating the marker a new object AR.GeoObject will be created at the specified geolocation. An AR.GeoObject connects one or more AR.GeoLocations with multiple AR.Drawables. The AR.Drawables can be defined for multiple targets. A target can be the camera, the radar or a direction indicator. Both the radar and direction indicators will be covered in more detail in later examples.
    */

    //alert(JSON.stringify(poiData));

    this.poiData = poiData;

    // create the AR.GeoLocation from the poi data
    var markerLocation = new AR.RelativeLocation(null, poiData.north, poiData.east, poiData.altitude);

    // create an AR.ImageDrawable for the marker in idle state
    var imgTemp = new AR.ImageResource(poiData.immagineIndicatore);
    //var overlay = new AR.ImageDrawable(imgTemp, poiData.dimensioneIndicatore,
    var overlay = new AR.ImageDrawable(imgTemp, 0.5,
    {
        enabled: true,
        opacity : 1.0,
        zOrder: 10
    });
    overlay.onClick = MarkerIndoor.prototype.mostraContenuti(poiData.id);
    overlay.customType=-1;
    overlay.idPoi=poiData.id;

    /*
    this.markerDrawable_idle = new AR.ImageDrawable(World.markerDrawable_idle, 2.5, {
        zOrder: 0,
        opacity: 1.0,
        onClick: Marker.prototype.getOnClickTrigger(this)
    });
*/

/*
    // create an AR.ImageDrawable for the marker in selected state
    this.markerDrawable_selected = new AR.ImageDrawable(World.markerDrawable_selected, 2.5, {
        zOrder: 0,
        opacity: 0.0,
        onClick: null
    });
*/
    
    //se le label sono aggiornate dinamicamente, non le metto, sarà la procedura che le disegnerà....
    var label = null;
    if (poiData.label==null)
    {
        //alert("label null");
    }
    else
    {
        // create an AR.Label for the marker's title 
        var label = new AR.Label(poiData.label, 0.5, {
            scale: 1,
            enabled: true,
            opacity : 0.5,
            zOrder: 15,
            style : {
                textColor : '#000000',
                backgroundColor : '#FFFFFF'
            }
        });
        label.onClick = MarkerIndoor.prototype.mostraContenuti(poiData.id);
        label.customType=-1;
        label.idPoi=poiData.id;

        var offsetGeneraleInizialeX=0;
        var offsetGeneraleInizialeY=0;

        if (poiData.labelPosition=="up")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="down")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.CENTER;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX;
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="sx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
        else if (poiData.labelPosition=="dx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
        else if (poiData.labelPosition=="upDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="downDx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="downSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.TOP;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY-(poiData.dimensioneIndicatore*0.6);
        }
        else if (poiData.labelPosition=="upSx")
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.BOTTOM;
            label.translate.x=offsetGeneraleInizialeX-(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY+(poiData.dimensioneIndicatore*0.6);
        }
        else //di default a destra
        {
            label.horizontalAnchor=AR.CONST.HORIZONTAL_ANCHOR.LEFT;
            label.verticalAnchor=AR.CONST.VERTICAL_ANCHOR.MIDDLE;
            label.translate.x=offsetGeneraleInizialeX+(poiData.dimensioneIndicatore*0.6);
            label.translate.y=offsetGeneraleInizialeY;
        }
    }


/*
    this.titleLabel = new AR.Label(poiData.title.trunc(10), 1, {
        zOrder: 1,
        translate: {
            y: 0.55
        },
        style: {
            textColor: '#FFFFFF',
            fontStyle: AR.CONST.FONT_STYLE.BOLD
        }
    });

    // create an AR.Label for the marker's description
    this.descriptionLabel = new AR.Label(poiData.description.trunc(15), 0.8, {
        zOrder: 1,
        translate: {
            y: -0.55
        },
        style: {
            textColor: '#FFFFFF'
        }
    });
*/

    // create the AR.GeoObject with the drawable objects
    if (label==null)
    {
        this.markerObject = new AR.GeoObject(markerLocation, {
            drawables: {
                cam: [overlay]
            }
        });
    }
    else
    {
        this.markerObject = new AR.GeoObject(markerLocation, {
            drawables: {
                cam: [overlay, label]
            }
        });
    }

    return this;
}


MarkerIndoor.prototype.mostraContenuti = function (idPoi)
{
    return function() {
        console.log("sono in mostraContenuti, con indicePoi: "+idPoi);
        //console.log("dati: " +JSON.stringify(oggetto.target[indiceImmagine].punti[indiceElemento]));
        showContenuto(idPoi);

    };
};


MarkerIndoor.prototype.getOnClickTrigger = function(marker) {

    /*
        The setSelected and setDeselected functions are prototype Marker functions.

        Both functions perform the same steps but inverted, hence only one function (setSelected) is covered in detail. Three steps are necessary to select the marker. First the state will be set appropriately. Second the background drawable will be enabled and the standard background disabled. This is done by setting the opacity property to 1.0 for the visible state and to 0.0 for an invisible state. Third the onClick function is set only for the background drawable of the selected marker.
    */

    return function() {

        if (marker.isSelected) {

            MarkerIndoor.prototype.setDeselected(marker);

        } else {
            MarkerIndoor.prototype.setSelected(marker);
            try {
                World.onMarkerSelected(marker);
            } catch (err) {
                alert(err);
            }

        }

        return true;
    };
};

MarkerIndoor.prototype.setSelected = function(marker) {

    marker.isSelected = true;

    marker.markerDrawable_idle.opacity = 0.0;
    marker.markerDrawable_selected.opacity = 1.0;
    marker.markerDrawable_idle.onClick = null;
    marker.markerDrawable_selected.onClick = MarkerIndoor.prototype.getOnClickTrigger(marker);
};

MarkerIndoor.prototype.setDeselected = function(marker) {

    marker.isSelected = false;

    marker.markerDrawable_idle.opacity = 1.0;
    marker.markerDrawable_selected.opacity = 0.0;

    marker.markerDrawable_idle.onClick = MarkerIndoor.prototype.getOnClickTrigger(marker);
    marker.markerDrawable_selected.onClick = null;
};

// will truncate all strings longer than given max-length "n". e.g. "foobar".trunc(3) -> "foo..."
String.prototype.trunc = function(n) {
    return this.substr(0, n - 1) + (this.length > n ? '...' : '');
};